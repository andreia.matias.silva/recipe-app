<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Api')->middleware('json')->group(function () {
    Route::resource('recipes', 'RecipeController', ['except' => ['destroy']]);

    Route::prefix('recipes/{recipe}')->group(function () {
        Route::resource('rating', 'RatingController', ['only' => ['store']]);
    });
});
