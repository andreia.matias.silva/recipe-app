<?php

namespace Tests;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Maatwebsite\Excel\Facades\Excel;

abstract class TestCase extends BaseTestCase
{
    const SUFFIX = '.backup';

    use CreatesApplication;


    public function setUp()
    {
        parent::setUp();

        $recipePath = $this->getRecipeFile();
        copy($recipePath, $recipePath . self::SUFFIX);

        $ratingPath = $this->getRatingsFile();
        copy($ratingPath, $ratingPath . self::SUFFIX);
    }

    public function tearDown()
    {
        $recipePath = $this->getRecipeFile();
        rename($recipePath . self::SUFFIX, $recipePath);

        $ratingPath = $this->getRatingsFile();
        rename($ratingPath . self::SUFFIX, $ratingPath);

        parent::tearDown();
    }

    protected function getRecipeFile()
    {
        return storage_path(env('RECIPE_FILE') . '.csv');
    }

    protected function getRatingsFile()
    {
        return storage_path(env('RATING_FILE') . '.csv');
    }

    protected function countInFile(string $file, string $class, Model $row)
    {
        $data = Excel::load($file)->get()->toArray();
        //dd(json_encode($data) . '=====' . json_encode($row));
        return collect($data)
            ->mapInto($class)
            ->filter(function ($item) use ($row) {
                return $row == $item;
            })
            ->count();
    }
}
