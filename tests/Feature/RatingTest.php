<?php

namespace Tests\Feature;

use App\Models\Rating;
use Illuminate\Http\Response;
use Tests\TestCase;

class RatingTest extends TestCase
{

    public function testAddValidRating()
    {
        $rating = factory(Rating::class)->make();

        $response = $this
            ->json('POST', '/api/recipes/1/rating', $rating->toArray())
            ->assertStatus(Response::HTTP_OK)
            ->decodeResponseJson();

        $rating['id'] = array_get($response, 'data.id');
        $rating['recipe_id'] = 1;
        $rating['created_at'] = array_get($response, 'data.created_at');
        $rating['updated_at'] = array_get($response, 'data.updated_at');

        $this->assertEquals(
            1,
            $this->countInFile($this->getRatingsFile(), Rating::class, $rating)
        );
    }

    public function testAddInvalidRating()
    {
        $response = $this->json('POST', '/api/recipes/1/rating', ['rating' => 6]);

        $response
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
