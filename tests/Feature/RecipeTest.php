<?php

namespace Tests\Feature;

use App\Models\Recipe;
use Illuminate\Http\Response;
use Tests\TestCase;

class RecipeTest extends TestCase
{

    public function testGetRecipeSuccess()
    {
        $response = $this->json('GET', '/api/recipes/123');

        $response
            ->assertJson([
                'data' => [
                    'id' => 123
                ]
            ])
            ->assertStatus(Response::HTTP_OK);
    }

    public function testGetRecipeShouldFail()
    {
        $response = $this->json('GET', '/api/recipes/666');

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testGetRecipesWithFilter()
    {
        $response = $this->json('GET', '/api/recipes?recipe_cuisine=asian&page=0&size=4');

        $response
            ->assertJson([
                'data' => [
                    [
                        'recipe_cuisine' => 'asian'
                    ]
                ]
            ])
            ->assertStatus(Response::HTTP_OK);
    }

    public function testGetRecipesWithPagination()
    {
        $response = $this->json('GET', '/api/recipes?page=0&size=1');

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testUpdateRecipe()
    {
        $recipe = factory(Recipe::class)->make();

        $response = $this->json('PUT', '/api/recipes/123', $recipe->toArray());

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testStoreRecipe()
    {
        $recipe = factory(Recipe::class)->make();

        $response = $this
            ->json('POST', '/api/recipes', $recipe->toArray())
            ->assertStatus(Response::HTTP_CREATED)
            ->decodeResponseJson();

        $recipe['id'] = array_get($response, 'data.id');
        $recipe['created_at'] = array_get($response, 'data.created_at');
        $recipe['updated_at'] = array_get($response, 'data.updated_at');

        $this->assertEquals(
            1,
            $this->countInFile($this->getRecipeFile(), Recipe::class, $recipe)
        );

    }
}
