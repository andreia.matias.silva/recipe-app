<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Recipe::class, function (Faker $faker) {
    return [
        'created_at' => $faker->dateTime(),
        'updated_at' => $faker->dateTime(),
        'base' => $faker->realText(15),
        'box_type' => $faker->realText(15),
        'bulletpoint1' => $faker->optional(0.5)->realText(15),
        'bulletpoint2' => $faker->optional(0.5)->realText(15),
        'bulletpoint3' => $faker->optional(0.5)->realText(15),
        'calories_kcal' => $faker->numberBetween(0, 800),
        'carbs_grams' => $faker->numberBetween(0, 100),
        'equipment_needed' => $faker->realText(15),
        'fat_grams' => $faker->numberBetween(0, 800),
        'gousto_reference' => $faker->numberBetween(0, 1000),
        'marketing_description' => $faker->realText(250),
        'origin_country' => $faker->country,
        'preparation_time_minutes' => $faker->numberBetween(10, 200),
        'protein_grams' => $faker->numberBetween(0, 800),
        'protein_source' => $faker->realText(10),
        'recipe_cuisine' => $faker->realText(10),
        'recipe_diet_type_id' => $faker->realText(10),
        'season' => $faker->realText(10),
        'shelf_life_days' => $faker->numberBetween(0, 40),
        'slug' => $faker->slug,
        'title' => $faker->realText(18),
        'short_title' => $faker->realText(10),
        'in_your_box' => $faker->realText(20),
    ];
});
