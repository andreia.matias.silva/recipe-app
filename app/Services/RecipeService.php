<?php

namespace App\Services;

use App\Models\Recipe;
use Carbon\Carbon;

class RecipeService
{
    protected $recipes, $manager;

    public function __construct()
    {
        $this->manager = new CsvDataManager(
            env('RECIPE_FILE'),
            Recipe::class,
            $this->fields()
        );
    }

    protected function fields()
    {
        return ['id',
            'created_at',
            'updated_at',
            'box_type',
            'title',
            'slug',
            'short_title',
            'marketing_description',
            'calories_kcal',
            'protein_grams',
            'fat_grams',
            'carbs_grams',
            'bulletpoint1',
            'bulletpoint2',
            'bulletpoint3',
            'recipe_diet_type_id',
            'season',
            'base',
            'protein_source',
            'preparation_time_minutes',
            'shelf_life_days',
            'equipment_needed',
            'origin_country',
            'recipe_cuisine',
            'in_your_box',
            'gousto_reference'
        ];
    }

    public function paginate(int $page, int $size, array $filters = [])
    {
        return $this->manager->paginate($page, $size, $filters);
    }

    public function find($id)
    {
        return $this->manager->find($id);
    }

    public function create(Recipe $recipe)
    {
        $recipe->created_at = Carbon::now()->format('d/m/Y H:i:m');
        $recipe->updated_at = Carbon::now()->format('d/m/Y H:i:m');

        return $this->manager->create($recipe);
    }

    public function update(Recipe $recipe, int $id)
    {
        $recipe->updated_at = Carbon::now()->format('d/m/Y H:i:m');

        return $this->manager->update($recipe, $id);
    }
}