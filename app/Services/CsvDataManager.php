<?php

namespace App\Services;

use App\Contracts\DataManager;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CsvDataManager implements DataManager
{
    private $filename;
    private $class;
    private $fields;
    private $data;
    private $primaryKeyName;

    public function __construct(string $filename, string $class, array $fields)
    {
        $this->filename = $filename;
        $this->class = $class;
        $this->fields = $fields;
        $this->primaryKeyName = (new $this->class())->getKeyName();
        $this->load();
    }

    public function all()
    {
        return $this->data;
    }

    public function paginate(int $page, int $size, array $filters = [])
    {
        $result = $this->data;

        if (!empty($filters)) {
            foreach ($filters as $key => $filter) {
                $result = $result->where($key, $filter);
            }
        }

        return $result->splice($page * $size, $size);
    }

    public function find($key)
    {
        $result = $this
            ->data
            ->where($this->primaryKeyName, $key);

        if ($result->isEmpty()) {
            throw new NotFoundHttpException();
        }

        return $result->first();
    }

    public function create(Model $model)
    {
        $max = $this->data->pluck($this->primaryKeyName)->max();
        $model[$this->primaryKeyName] = empty($max) ? 1 : $max + 1;

        $this->data->push($model);

        $this->saveFile();
        return $model;
    }

    public function update(Model $model, $key)
    {
        $model = $this->find($key);

        $this->data = $this->data->filter(function ($obj) use ($model) {
            return $obj[$this->primaryKeyName] !== $model->getKey();
        });

        $this->data->push($model);

        $this->saveFile();

        return $model;
    }

    protected function load()
    {
        $data = Excel::load(storage_path($this->filename . '.csv'))->get()->toArray();

        $this->data = collect($data)->mapInto($this->class);
    }

    protected function saveFile()
    {
        Excel::create($this->filename, function ($excel) {
            $excel->sheet("sheet", function ($sheet) {
                $sheet->fromArray($this->getCsvData(), null, 'A1', false, false);
            });
        })->save('csv', storage_path());
    }

    protected function getCsvData(): array
    {
        $csv[] = $this->fields;

        foreach ($this->data->toArray() as $row) {
            $new = [];
            foreach ($this->fields as $field) {
                $info = array_get($row, $field);

                $new[] = $info;
            }

            $csv[] = $new;
        }
        return $csv;
    }

}