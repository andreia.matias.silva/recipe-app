<?php

namespace App\Services;

use App\Models\Rating;
use Carbon\Carbon;

class RatingService
{
    protected $manager;

    public function __construct()
    {
        $this->manager = new CsvDataManager(
            env('RATING_FILE'),
            Rating::class,
            $this->fields()
        );
    }

    protected function fields()
    {
        return [
            'id',
            'rating',
            'recipe_id',
            'updated_at',
            'created_at'
        ];
    }

    public function create(Rating $rating, int $recipe)
    {
        $rating->recipe_id = $recipe;
        $rating->created_at = Carbon::now()->format('d/m/Y H:i:m');
        $rating->updated_at = Carbon::now()->format('d/m/Y H:i:m');

        return $this->manager->create($rating);
    }
}