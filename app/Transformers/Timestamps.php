<?php

namespace App\Transformers;

trait Timestamps
{
    /**
     * Turn this item object into a generic array including additional data.
     *
     * @param  $item
     * @param array $data
     * @return array
     */
    public function withTimestamps($item, array $data = [])
    {
        return array_merge($data, [
            'created_at' => $this->formatDate(data_get($item, 'created_at')),
            'updated_at' => $this->formatDate(data_get($item, 'updated_at'))
        ]);
    }
}
