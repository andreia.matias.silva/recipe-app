<?php

namespace App\Transformers;

use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class Transformer extends TransformerAbstract
{
    public function formatDate(?Carbon $date)
    {
        if (is_null($date)) {
            return null;
        }

        return $date->format('d/m/Y H:i:s');
    }
}
