<?php

namespace App\Transformers;

use App\Models\Recipe;
use League\Fractal\Resource\NullResource;

class RecipeTransformer extends Transformer
{
    use Timestamps;

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
        'in_your_box',
    ];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
        'ratings'
    ];

    /**
     * Turn this item object into a generic array.
     *
     * @param Recipe $recipe
     * @return array
     */
    public function transform(Recipe $recipe)
    {
        return $this->withTimestamps($recipe, [
            'id' => $recipe->id,
            'base' => $recipe->base,
            'box_type' => $recipe->box_type,
            'bulletpoint1' => $recipe->bulletpoint1,
            'bulletpoint2' => $recipe->bulletpoint2,
            'bulletpoint3' => $recipe->bulletpoint3,
            'calories_kcal' => $recipe->calories_kcal,
            'carbs_grams' => $recipe->carbs_grams,
            'equipment_needed' => $recipe->equipment_needed,
            'fat_grams' => $recipe->fat_grams,
            'gousto_reference' => $recipe->gousto_reference,
            'marketing_description' => $recipe->marketing_description,
            'origin_country' => $recipe->origin_country,
            'preparation_time_minutes' => $recipe->preparation_time_minutes,
            'protein_grams' => $recipe->protein_grams,
            'protein_source' => $recipe->protein_source,
            'recipe_cuisine' => $recipe->recipe_cuisine,
            'recipe_diet_type_id' => $recipe->recipe_diet_type_id,
            'season' => $recipe->season,
            'shelf_life_days' => $recipe->shelf_life_days,
            'slug' => $recipe->slug,
            'title' => $recipe->title,
        ]);
    }

    /**
     * Include in_your_box.
     *
     * @param Recipe $recipe
     * @return \League\Fractal\Resource\Item|NullResource
     */
    public function includeInYourBox(Recipe $recipe)
    {
        $data = explode(', ', $recipe->in_your_box);

        if (collect($data)->filter()->isEmpty()) {
            return new  NullResource();
        }

        return $this->item(
            $data,
            new ArrayTransformer,
            'in_your_box'
        );
    }

    /**
     * Include ratings.
     *
     * @param Recipe $recipe
     * @return \League\Fractal\Resource\Collection
     */
    public function includeRatings(Recipe $recipe)
    {
        return $this->collection(
            $recipe->ratings,
            new RatingTransformer,
            'ratings'
        );
    }
}
