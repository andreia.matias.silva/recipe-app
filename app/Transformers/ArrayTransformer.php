<?php

namespace App\Transformers;


class ArrayTransformer extends Transformer
{
    /**
     * @param array $array
     * @return array
     */
    public function transform(array $array)
    {
        return $array;
    }
}
