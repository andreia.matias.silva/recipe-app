<?php

namespace App\Transformers;


use App\Models\Rating;

class RatingTransformer extends Transformer
{
    use Timestamps;

    /**
     * Turn this item object into a generic array.
     *
     * @param Rating $rating
     * @return array
     */
    public function transform(Rating $rating)
    {
        return $this->withTimestamps($rating, [
            'id' => $rating->id,
            'rating' => $rating->rating,
        ]);
    }
}
