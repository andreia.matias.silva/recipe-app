<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    /**
     * The attributes that are protected against mass assignment.
     *
     * @var array
     */
    protected $guarded = [];

    protected $dateFormat = 'd/m/Y H:i:s';

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'carbs_grams' => 'integer',
        'calories_kcal' => 'integer',
        'fat_grams' => 'integer',
        'gousto_reference' => 'integer',
        'preparation_time_minutes' => 'integer',
        'protein_grams' => 'integer',
        'shelf_life_days' => 'integer'
    ];

    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }
}
