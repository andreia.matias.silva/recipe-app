<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller as BaseController;
use League\Fractal\Manager;

class Controller extends BaseController
{

    protected $interface;

    /**
     * get a fractal services instance.
     *
     */
    protected function getService()
    {
        return new Manager();
    }

    /**
     * get a fractal services instance.
     *
     */
    protected function process($resource)
    {
        return $this->getService()->createData($resource);
    }

}
