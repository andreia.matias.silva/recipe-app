<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Recipe as Requests;
use App\Models\Recipe;
use App\Services\RecipeService;
use App\Transformers\RecipeTransformer;
use Illuminate\Http\Response;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

class RecipeController extends Controller
{
    protected $service;

    public function __construct()
    {
        $this->service = new RecipeService();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Requests\IndexRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Requests\IndexRequest $request)
    {
        $array = $this->service->paginate(
            $request->get('page'),
            $request->get('size'),
            $request->except('page', 'size')
        );

        return response()
            ->json($this->process(new Collection($array, new RecipeTransformer()))->toArray())
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @param Requests\ShowRequest $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show(int $id, Requests\ShowRequest $request)
    {
        $recipe = $this->service->find($id);

        return response()
            ->json($this->process(new Item($recipe, new RecipeTransformer()))->toArray())
            ->setStatusCode(Response::HTTP_OK);
    }

    /**
     * Create the specified resource.
     *
     * @param Requests\StoreRequest $request
     * @return mixed
     */
    public function store(Requests\StoreRequest $request)
    {
        $recipe = new Recipe();
        $recipe->fill($request->all());
        $recipe = $this->service->create($recipe);

        return response()
            ->json($this->process(new Item($recipe, new RecipeTransformer()))->toArray())
            ->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Update the specified resource.
     *
     * @param int $id
     * @param Requests\StoreRequest $request
     * @return mixed
     */
    public function update(int $id, Requests\StoreRequest $request)
    {
        $recipe = new Recipe();
        $recipe->fill($request->all());
        $recipe = $this->service->update($recipe, $id);

        return response()
            ->json($this->process(new Item($recipe, new RecipeTransformer()))->toArray())
            ->setStatusCode(Response::HTTP_OK);
    }

}
