<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Rating as Requests;
use App\Models\Rating;
use App\Services\RatingService;
use App\Transformers\RatingTransformer;
use Illuminate\Http\Response;
use League\Fractal\Resource\Item;

class RatingController extends Controller
{
    protected $service;

    public function __construct()
    {
        $this->service = new RatingService();
    }

    /**
     * Create the specified resource.
     *
     * @param int $recipe
     * @param Requests\StoreRequest $request
     * @return mixed
     */
    public function store(int $recipe, Requests\StoreRequest $request)
    {
        $rating = new Rating();
        $rating->fill($request->all());
        $response = $this->service->create($rating, $recipe);

        return response()
            ->json($this->process(new Item($response, new RatingTransformer()))->toArray())
            ->setStatusCode(Response::HTTP_OK);
    }
}
