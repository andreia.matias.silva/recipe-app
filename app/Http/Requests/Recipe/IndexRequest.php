<?php

namespace App\Http\Requests\Recipe;

use App\Http\Requests\BaseRequest;

class IndexRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Define relations to handle automatically.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'page' => 'required|integer',
            'size' => 'required|integer',
            'includes' => 'nullable|array',
        ];
    }
}
