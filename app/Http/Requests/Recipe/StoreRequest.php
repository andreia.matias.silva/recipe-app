<?php

namespace App\Http\Requests\Recipe;

use App\Http\Requests\BaseRequest;

class StoreRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Define relations to handle automatically.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'base' => 'nullable|string',
            'box_type' => 'required|string',
            'bulletpoint1' => 'nullable|string',
            'bulletpoint2' => 'nullable|string',
            'bulletpoint3' => 'nullable|string',
            'calories_kcal' => 'required|numeric',
            'carbs_grams' => 'required|numeric',
            'equipment_needed' => 'required|string',
            'fat_grams' => 'required|numeric',
            'in_your_box' => 'nullable|string',
            'gousto_reference' => 'required|numeric',
            'marketing_description' => 'required|string',
            'origin_country' => 'required|string',
            'preparation_time_minutes' => 'required|numeric',
            'protein_source' => 'required|string',
            'protein_grams' => 'required|numeric',
            'recipe_cuisine' => 'required|string',
            'recipe_diet_type_id' => 'required|string',
            'season' => 'required|string',
            'shelf_life_days' => 'required|numeric',
            'short_title' => 'nullable|string',
            'slug' => 'required|string',
            'title' => 'required|string',
        ];
    }
}
