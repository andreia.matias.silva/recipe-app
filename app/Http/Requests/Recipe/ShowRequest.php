<?php

namespace App\Http\Requests\Recipe;

use App\Http\Requests\BaseRequest;

class ShowRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Define relations to handle automatically.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'includes' => 'nullable|array',
        ];
    }
}
