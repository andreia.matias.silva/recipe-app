<?php

namespace App\Contracts;

use Illuminate\Database\Eloquent\Model;

interface DataManager
{
    public function all();

    public function paginate(int $page, int $size, array $filters = []);

    public function find($key);

    public function create(Model $model);

    public function update(Model $model, $key);

}