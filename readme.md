# Gousto Recipe Backend Test

## Getting Started

You will require PHP 7.X to run this app.
If you don't have this version on your system you easily setup a local development environment use [Homestead](https://laravel.com/docs/5.6/homestead).

Clone or extract this project and in the project folder do:

```
composer install
```

Copy the `env.example` rename it to `.env` and update the next env variables:

```
RECIPE_FILE=data\recipe-data.csv
RATING_FILE=data\rating-data.csv
```

If you are running PHP on your local machine:

```
php artisan serve
```
otherwise, if you're using Homestead, you can skip this.

You can check if the application is running at: [http://127.0.0.1:8000](http://127.0.0.1:8000)

Note: if you are using Homestead use the correct hostname.

### Tests

To run the tests use `phpunit` on the project folder.

## How to use?

1. Fetch a recipe by id
```
curl -X GET \
  http://127.0.0.1:8000/api/recipes/1 \
  -H 'Content-Type: application/json'
```

2. Fetch all recipes for a specific cuisine (should paginate)
```
curl -X GET \
  'http://127.0.0.1:8000/api/recipes?recipe_cuisine=british&page=1&size=1' \
  -H 'Content-Type: application/json'
```

3. Rate an existing recipe between 1 and 5
``` 
curl -X POST \
  http://gousto.test/api/recipes/1/rating \
  -H 'Content-Type: application/json' \
  -d '{"rating":5}'
```

4. Store a new recipe
```
curl -X POST \
  http://127.0.0.1:8000/api/recipes \
  -H 'Content-Type: application/json' \
  -d '{     
            "base": "rice",
            "box_type": "paleo",
            "bulletpoint1": null,
            "bulletpoint2": null,
            "bulletpoint3": null,
            "calories_kcal": 401,
            "carbs_grams": 2,
            "equipment_needed": "Appetite",
            "fat_grams": 35,
            "gousto_reference": 18,
            "marketing_description": "Here we'\''ve used onglet steak which is an extra flavoursome cut of beef that should never be cooked past medium rare. So if you'\''re a fan of well done steak, this one may not be for you. However, if you love rare steak and fancy trying a new cut, please be",
            "origin_country": "Portugal",
            "preparation_time_minutes": 35,
            "protein_grams": 12,
            "protein_source": "cod",
            "recipe_cuisine": "mediterranean",
            "recipe_diet_type_id": "fish",
            "season": "all",
            "shelf_life_days": 4,
            "slug": "bacalhau-bras",
            "title": "Bacalhau a Bras",
            "in_your_box":"cod, potato, onion, garlic"
}'
```

5. Update an existing recipe
```
curl -X PUT \
  http://127.0.0.1:8000/api/recipes/11 \
  -H 'Content-Type: application/json' \
  -d '{     
            "base": "potato",
            "box_type": "paleo",
            "bulletpoint1": null,
            "bulletpoint2": null,
            "bulletpoint3": null,
            "calories_kcal": 401,
            "carbs_grams": 2,
            "equipment_needed": "Appetite",
            "fat_grams": 35,
            "gousto_reference": 18,
            "marketing_description": "Here we'\''ve used onglet steak which is an extra flavoursome cut of beef that should never be cooked past medium rare. So if you'\''re a fan of well done steak, this one may not be for you. However, if you love rare steak and fancy trying a new cut, please be",
            "origin_country": "Portugal",
            "preparation_time_minutes": 35,
            "protein_grams": 12,
            "protein_source": "cod",
            "recipe_cuisine": "mediterranean",
            "recipe_diet_type_id": "fish",
            "season": "all",
            "shelf_life_days": 4,
            "slug": "bacalhau-bras",
            "title": "Bacalhau a Bras",
            "in_your_box":"cod, potato, onion, garlic"
}'
```

## Why Laravel

The main reason was because, looking at the job description, it would be desirable experience in Laravel and 
it would be beneficial to demonstrate my experience. 
This might be overkill for a simple project like this and maybe lighter frameworks like Lumen would be more than enough.

## Solution

In this project each REST resource has a controller and for each HTTP method handler there's a
`Request Form Validator`. The controllers interact with services that
exposes the business rules and in turn use a `DataManager` to persist and access data.
This manager hides the details of the underlying persistence mechanisms.

Before the response is sent, there's one last step which requires a `Transformer` to do some response manipulation
like restructuring the response types.  

Regarding the tests, there's only end to end tests that insure the correct behaviour of
each API endpoint. 

## Future Improvements

1. API authentication
2. Allow the request to set the `includes` and `excludes` for the transformers
3. Use a database
4. Unit and integration tests

###This solution would cater different API consumers (mobile, web)?

No yet, doing so would only require us to do the improvement #2. This would allow the consumer to strip the response from data
that is not required. 
